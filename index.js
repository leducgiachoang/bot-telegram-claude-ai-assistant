const { Telegraf } = require('telegraf');
const { message } = require('telegraf/filters');

const { handleMessages, debounce } = require('./controller')
const {YOUR_TOKEN_TELEGRAM} = require('./configs');

const bot = new Telegraf(YOUR_TOKEN_TELEGRAM);

bot.start((ctx) => ctx.reply("Welcome Claude AI BOT ✨✨"));
bot.help((ctx) => ctx.reply("Send me a sticker sd"));
bot.on(message('sticker'), (ctx) => ctx.reply(ctx.message.sticker));

bot.on('text', async (ctx) => {
    var message = ctx.message.text;
    if(message.indexOf('@chat') === 0){
        message = message.replace('@chat', '');
        const reply = await ctx.reply('Wait a moment...');
        debounce(handleMessages({
            ctx,
            message_id: reply.message_id,
            message
        }), 1000);
        return;
    }
    
});

bot.launch();

process.once('SIGINT', () => bot.stop('SIGINT'))
process.once('SIGTERM', () => bot.stop('SIGTERM'))