const {CLAUDE_MODEL, API_KEY_CLAUDE} = require('./configs');
const {Anthropic} = require('@anthropic-ai/sdk');
module.exports = {
    handleMessages: async ({ctx, message_id, message}) => {
        var anthropic = new Anthropic({
            apiKey: API_KEY_CLAUDE, // This is the default and can be omitted
        });
        const chat = await anthropic.messages.create({
            max_tokens: 1024,
            messages: [{role: 'user', content: message}],
            model: CLAUDE_MODEL,
        });
        await ctx.telegram.editMessageText(ctx.message.chat.id, message_id, null, chat.content[0].text + '\n \nBot created by GiacHoang. 😘😘');
    },
    debounce(fn, ms) {
        let timer;
        return function() {
            // Nhận các đối số
            const args = arguments;
            const context = this;
            if(timer) clearTimeout(timer);
            
            timer = setTimeout(() => {
                fn.apply(context, args);
            }, ms);
        }
    }
}